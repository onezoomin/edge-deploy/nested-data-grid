import type { fs } from 'webnative'
import { logChanges } from './datascript'
export let wnfs, wn, pathString, apDir, rowsDir, sqlDir, lsResults

export const initWebnative = async () => {
  wn = globalThis?.webnative
  if (!wn) return console.warn('missing wn')

  if (wnfs?.appPath()) {
    console.log(wnfs.ls(wnfs.appPath()))
    return wnfs
  }
  const state = await wn.initialise({
    permissions: {
      // Will ask the user permission to store
      // your apps data in `private/Apps/onezoomin/nested-data-grid`
      app: {
        name: 'nested-data-grid',
        creator: 'onezoomin',
      },

      // Ask the user permission for additional filesystem paths
      fs: {
        publicPaths: ['/'],
      },
    },
  })

  switch (state.scenario) {
    case wn.Scenario.AuthCancelled:
      // User was redirected to lobby,
      // but cancelled the authorisation
      break

    case wn.Scenario.AuthSucceeded:
    case wn.Scenario.Continuation:
      // State:
      // state.authenticated    -  Will always be `true` in these scenarios
      // state.newUser          -  If the user is new to Fission
      // state.throughLobby     -  If the user authenticated through the lobby, or just came back.
      // state.username         -  The user's username.
      //
      // ☞ We can now interact with our file system (more on that later)
      wnfs = state.fs
      globalThis.ucan = wn.ucan

      lsResults = await wnfs.ls(wnfs.appPath())
      rowsDir = wn.path.directory(...wnfs.appPath().directory, 'Rows')
      sqlDir = wn.path.directory(...wnfs.appPath().directory, 'Sql')

      if (!Object.keys(lsResults).includes('Rows')) {
        await wnfs.mkdir(rowsDir)
        lsResults = await wnfs.ls(wnfs.appPath())
        await wnfs.publish()
      }

      if (!Object.keys(lsResults).includes('Sql')) {
        await wnfs.mkdir(sqlDir)
        lsResults = await wnfs.ls(wnfs.appPath())
        await wnfs.publish()
      }

      const rowsLS = await wnfs.ls(rowsDir)
      console.log('first fs', { lsResults, rowsLS, state, apDir, pathString, wnfs, wn })

      break

    case wn.Scenario.NotAuthorised:
      wn.redirectToLobby(state.permissions)
      break
  }
  return wnfs
}

void initWebnative()

const tracker = {
  adds: [],
  addsAvg: 0,
  pubs: [],
  pubsAvg: 0,
}
export const writeToWNFS = async (content: any, fileName, pathObj = sqlDir) => {
  if (!wnfs) wnfs = await initWebnative()
  const rowsLS = await wnfs?.ls(pathObj) // ls needs an object with a directory key::  { directory: string[] }
  const firstLsEntry = Object.keys(rowsLS)[0]
  const overWritePath = firstLsEntry ? wn.path.file(...pathObj.directory, firstLsEntry) : null
  const filePath = wn.path.file(...pathObj.directory, fileName) // add will need  { file: string[] }
  console.log({ rowsLS, filePath })
  await wnfs.add(filePath, content)
  if (overWritePath) await wnfs.add(overWritePath, content)
  await wnfs.publish()
}
export const persistDataWebnative = async (dataStruct: any) => {
  if (!dataStruct?.rows?.length) return console.warn('bogus data', dataStruct)
  if (!wnfs) console.warn('trying to (re)load fs', await initWebnative())

  const addPromises = []
  for (let eachRow of dataStruct.rows) {
    eachRow = { ...eachRow }
    const { id } = eachRow
    delete eachRow.id

    /// a bunch of stuff that did not work
    // const filePath = [...(wnfs.appPath().directory), `${id}.json`]
    // const filePath = `${pathString}/${id}.json`
    // const filePath = wn.path.file(wn.path.Branch.Private, `${id}.json`)
    // const filePath = wnfs.appPath(['Rows',`${id}.json`])
    // const filePath = [...apDir, `${id}.json`]

    const rowsLS = await wnfs.ls(rowsDir) // ls needs an object with a directory key::  { directory: string[] }
    const filePath = wn.path.file(...rowsDir.directory, `${id}.json`) // add will need  { file: string[] }
    // console.log({ rowsLS, eachRow, filePath, wnfs })
    const jsonString = JSON.stringify(eachRow, null, 2)

    // addPromises.push( wnfs.add(filePath,jsonString))

    void logChanges(eachRow.elements)
  }
  // let p = performance.now()
  // await Promise.all(addPromises)
  // addPromises=[]
  // tracker.adds.push(performance.now()-p)
  // p = performance.now()
  // await wnfs.publish()
  // tracker.pubs.push(performance.now()-p)
  // tracker.addsAvg = tracker.adds.reduce((a, b) => a + b, 0) / tracker.adds.length
  // tracker.pubsAvg = tracker.pubs.reduce((a, b) => a + b, 0) / tracker.pubs.length
  // console.log(tracker);
}
