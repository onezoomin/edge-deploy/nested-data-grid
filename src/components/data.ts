import type { Model } from 'hybrids'
import { checkDB, doQuery } from './data/datascript'
import { TxVM } from './data/types'
import { wnfs, persistDataWebnative, writeToWNFS } from './data/webnative'
import { changeHue, changeLightness, randomHexColor } from './utils/color-utils'
import { utcMsTs } from './utils/utils'
const { store } = globalThis?.hybrids ?? {} // will be async imported in init.raw

interface Element {
  color: string
  isDeleted?: boolean
  lastUpdate: number
}

const Element: Model<Element> = {
  // id: true,
  color: randomHexColor(),
  isDeleted: false,
  lastUpdate: utcMsTs(),
  // [store.connect]: (id) => fetch(`/elements/${id}`).then(res => res.json()),
}
let r = 0; let eid = 0

const initColor = randomHexColor()
let ch = 0
const exampleRow = (lastUpdate = utcMsTs(), baseColor = changeHue(initColor, (ch += 36))) => ({
  id: r++,
  elements: [
    { id: eid++, color: baseColor, lastUpdate },
    { id: eid++, color: changeLightness(baseColor, 1.1), lastUpdate },
    { id: eid++, color: changeLightness(baseColor, 1.3), lastUpdate },
    { id: eid++, color: changeLightness(baseColor, 1.5), lastUpdate },
  ],
})
export interface Row {
  id: any
  elements: Element[]
}

const Row: Model<Row> = {
  // id: true,
  elements: [Element, { loose: true }],
}
const ColData = {
  // id: true,
  rows: [Row, { loose: true }],
}

const ColDataStore = store.get(ColData)

// const ColStruct = {
//   rows: [
//     exampleRow(),
//     exampleRow(),
//   ]
// }
// store.set(ColData,ColStruct)

const ColStruct2 = {
  rows: [
    // ...(store.get(ColData).rows),
    exampleRow(),
    exampleRow(),
    exampleRow(),
    exampleRow(),
    // exampleRow(),
    // exampleRow(),
  ],
}

store.set(ColData, ColStruct2)
console.log(store.get(ColData).rows)

export { Element, ColData, ColDataStore }

export const randomChange = () => {
  const { rows } = ColStruct2
  const op = ['create', 'update', 'update', 'delete'][Math.floor(Math.random() * 4)]
  const rowIndex = Math.floor(Math.random() * rows.length).toString()
  const elIndex = Math.floor(Math.random() * (rows[rowIndex].elements.length - 1))
  const { id, color: elcolor } = rows[rowIndex].elements[elIndex]
  switch (op) {
    case 'create':
      const color = randomHexColor()
      rows[rowIndex].elements.push({
        id: eid++,
        color,
        lastUpdate: utcMsTs(),
      })
      store.set(ColData, ColStruct2)
      return console.log('create', { rowIndex, elIndex, ColStruct2 })
    case 'update':

      ColStruct2.rows[rowIndex].elements[elIndex] = { id, color: changeHue(elcolor, 120), lastUpdate: utcMsTs() }
      store.set(ColData, ColStruct2)
      return console.log('update', { rowIndex, elIndex, ColStruct2 })
    case 'delete':
      ColStruct2.rows[rowIndex].elements[elIndex] = { id, isDeleted: true, color: '-deleted-', lastUpdate: utcMsTs() }
      store.set(ColData, ColStruct2)
      return console.log('delete', { rowIndex, elIndex, rows })

    default:
      return console.log('no-op')
  }
}

const startTS = Date.now()
const ENOUGH = 10000
let lsResults, fsRef, didQueryOnce
const updateInterval = setInterval(() => {
  void (async () => {
    randomChange()
    // const ColDataStore = store.get(ColData)
    if (wnfs && !fsRef) {
      fsRef = wnfs
      lsResults = await wnfs.ls(wnfs.appPath())
      console.log({ fs: wnfs, lsResults })
    }
    if (fsRef) {
      await persistDataWebnative(ColStruct2)
    }
    const ds = globalThis.datascript
    if (!didQueryOnce && globalThis.datascript) {
      console.log('ds has arrived', { ds })
      const qRes = doQuery()
      console.log({ qRes })
      checkDB()
      didQueryOnce = true
    }
    const isEnoughForNow = ((Date.now() - startTS) > ENOUGH)
    if (isEnoughForNow) {
      const Buffer = (await import('buffer/')).Buffer
      clearInterval(updateInterval)
      const SQLdb = globalThis.SQLdb
      const Uint8Arr = SQLdb.export()
      const sqlBuffer = Buffer.from(Uint8Arr)
      const fileName = `${Date.now()}.sqlite3`
      void writeToWNFS(sqlBuffer, fileName)
      const [{ columns, values }] = SQLdb.exec('SELECT * FROM txs')
      const vmTest = new TxVM(values[0])
      console.log({ vmTest, columns, values })
      const selRes = []
      for (const eachValArr of values) {
        const eachObj = {}
        for (const eachIdx in eachValArr) eachObj[columns[eachIdx]] = eachValArr[eachIdx]
        selRes.push(eachObj)
      }
      console.table(selRes)
    }
  })()
}, 1000)
